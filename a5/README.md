> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Assignment 5 Requirements:

*3 requirements:*

1. Screenshots of MS SQL server ERD (1st half)
2. Screenshots of some SQL queries
3. Class Bitbucket repo link


#### README.md file should include the following items:

* Screenshots of sql code 
* Screenshot of ERD
* Class repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of SQL code http://localhost*:

![A5 table creation code Screenshot](img/a5_sql_1.png)
![A5 table creation code Screenshot](img/a5_sql_2.png)
![A5 table creation code Screenshot](img/a5_sql_3.png)


*Screenshot of ERD*:

![A5 ERD](img/a5erd.png)

*Screenshot of SQL queries*:
![A5 ERD](img/a5_query1.png)
![A5 ERD](img/a5_query2.png)



#### Links:

*My 3781 class repository*
[3781 repo](https://bitbucket.org/arg19e/lis3781/src/master/ "My class repository")