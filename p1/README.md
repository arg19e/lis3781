> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Project 1 Requirements:

*4 requirements:*

1. ERD in .mwb format
2. Screenshot of person table
3. SQL statement answers
4. Class Bitbucket repo link


#### README.md file should include the following items:

* Screenshot of erd
* Screenshot of person table
* Answers to SQL questions
* Class repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project Screenshots:

*Screenshots of erd http://localhost*:

![ERD screenshot](img/erd_p1.png)

*Screenshot of table contents*:

![person table](img/person_p1.png)

*Answers to a few of the SQL questions*
![Some of the SQL answers](img/que.png)

#### Links:

*My 3781 class repository*
[3781 repo](https://bitbucket.org/arg19e/lis3781/src/master/ "My class repository") 