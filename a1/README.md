> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Assignment 1 Requirements:

*3 requirements:*
1. git excercise
2. erd screenshot
3. excercise 1 sql solution


#### README.md file should include the following items:

* git excercises
* screenshot of erd
* screenshot of sql solution


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates new git repository
2. git status - displays state of the working directory and the staging area
3. git add - adds a change in the working directory to the staging area
4. git commit - captures a snapshot of the project's currently staged changes
5. git push - to upload local repository content to a remote repository
6. git pull - used to fetch and download content from a remote repository and immediately update the local repository to match that content
7. additional command: git rm - removes files from the working tree and the index

#### Assignment Screenshots:

*Screenshot of A1 ERD http://localhost*:

![A1 ERD Screenshot](img/arg19e_A1.png)

*Screenshot of Ex1 SQL Solution*:

![Ex1 Screenshot](img/sol1.png)

*Screenshot of Ex2 SQL Solution*:

![Ex2 Screenshot](img/num2.png)

*Screenshot of Ex3 SQL Solution*:

![Ex3 Screenshot](img/num3.png)

*Screenshot of Ex4 SQL Solution*:

![Ex3 Screenshot](img/num4.png)


#### Links:
Completed tutorial repo:
[ repo from the tutorial](https://bitbucket.org/arg19e/bitbucketstationlocations/src/master/ )

My 3781 repo:
[ My 3781 Repository Link](https://bitbucket.org/arg19e/lis3781/src/master/ "My Team Quotes Tutorial")