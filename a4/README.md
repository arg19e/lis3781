> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Assignment 4 Requirements:

*3 requirements:*

1. Screenshots of MS SQL server ERD (1st half)
2. Screenshots of MS SQL server ERD (2nd half)
3. Class Bitbucket repo link


#### README.md file should include the following items:

* Screenshots of sql code 
* Screenshot of ERD
* Class repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of SQL code http://localhost*:

![A4 customer table creation code Screenshot](img/a4_person_table.png)
![A4 customer table insert and hashing code Screenshot](img/a4_person_insert.png)

*Screenshot of ERD*:

![A4 ERD part 1 ](img/a4_erd_part1.png)
![A4 ERD part 2 ](img/a4_erd_part2.png)


#### Links:

*My 3781 class repository*
[3781 repo](https://bitbucket.org/arg19e/lis3781/src/master/ "My class repository")