> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Alyssa Goosby 

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Git command excercise
    - ERD screenshot
    - SQL syntax screenshot

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshots of the SQL syntax for two table creations
    - Screenshots of the table in MS SQL Server
    - bit bucket repo link
    
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshots of the SQL syntax for two table creations
    - Screenshots of the table contents in Oracle
    - bit bucket repo link

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - erd in mwb format
    - SQL statements 
    - bit bucket repo link

    
5. [A4 README.md](a4/README.md "My A4 README.md file")
    - ERD screenshots
    - Creation and insert statements for person table 
    - bit bucket repo link

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - ERD screenshots
    - Table creation SQL statements 
    - SQL queries 
    - bit bucket repo link

6. [P2 README.md](p2/README.md "My P2 README.md file")
    - mongo command shell 
    - mongo question answers 1-4
    - bit bucket repo link