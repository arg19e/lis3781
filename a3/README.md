> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Assignment 3 Requirements:

*5 requirements:*

1. Screenshots of sql creation code for customer table
2. Screenshots of sql creation code for commodity table
3. Screenshots of sql creation code for order table
4. Screenshot of table contents
5. Class Bitbucket repo link


#### README.md file should include the following items:

* Screenshots of sql code 
* Screenshot of table contents
* Class repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of SQL code http://localhost*:

![A3 customer table code Screenshot](img/a3_cus.png)
![A3 commodity table code Screenshot](img/a3_comm.png)
![A3 order table code Screenshot](img/a3_order.png)
![A3 customer inserts Screenshot](img/a3_ord_ins.png)
![A3 comm inserts Screenshot](img/a3_ord_ins2.png)

*Screenshot of table contents*:

![A3 customer table contents](img/a3_cus_table.png)
![A3 commodity table contents](img/a3_commodity_table.png)
![A3 order table contents](img/a3_order_table.png)

#### Links:

*My 3781 class repository*
[3781 repo](https://bitbucket.org/arg19e/lis3781/src/master/ "My class repository")