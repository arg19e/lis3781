> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Project 2 Requirements:

*3 requirements:*

1. Screenshot of at least one MongoDB shell command
2. Screenshot: At least *one* required report (i.e., exercise below), and JSON code solution. 
3. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of mongo shell commands
* Screenshot of a few reports
* Class repo link

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Project Screenshots:

*Screenshots of mongo shell commands http://localhost*:

![Mongo shell commands](img/mon_commands.png)
![Mongo shell commands](img/mon_version.png)

*Screenshots of mongo shell commands http://localhost*:

![Question #1](img/mon_que1.png)
![Question #2](img/mon_que2.png)
![Question #2](img/mon_que3.png)
![Question #2](img/mon_que4.png)


#### Links:

*My 3781 class repository*
[3781 repo](https://bitbucket.org/arg19e/lis3781/src/master/ "My class repository") 