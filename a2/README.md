> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781

## Alyssa Goosby

### Assignment 2 Requirements:

*3 requirements:*

1. Screenshots of sql creation code for tables customer 
2. Screenshots of sql creation code for tables company
3. Screenshot of table contents


#### README.md file should include the following items:

* Screenshots of sql code 
* Screenshot of table contents


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of SQL code http://localhost*:

![A2 code Screenshot](img/code1.png)
![A2 code Screenshot](img/code2.png)
![A2 code Screenshot](img/code3.png)
![A2 code Screenshot](img/code4.png)

*Screenshot of table contents*:

![customer table results Screenshot](img/cus_result.png)
![company table results Screenshot](img/cmp_result.png)

#### Links:

*My 3781 class repository*
[3781 repo](https://bitbucket.org/arg19e/lis3781/src/master/ "My class repository")